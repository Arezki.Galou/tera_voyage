package BaseDeDonnee;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import outils.Database;
import outils.Table_Base_De_Donnee;

/***
 * 
 * <p>
 * <h3><strong> Class Resevation</strong>:</h3>
 * <h4>description</h4>
 * 	qui offre des services lier à la gestions des reservation des vols de l'application 
 * <h4>Service Offerts </h4>
 * 		<ul>
 * 			<li> Ajout une reservation  </li>
 * 			<li> Annuler une reservation </li>
 * 		</ul>
 * </p>
 * 
 * @author Kichou Galou Lounis
 *
 */
public class Reservation {

	/**
	 * 
	 * Service permettant d'ajouter un nouvelle reservation d'utilisateur à la base de données 
	 * @param pseudo : pseudo de l'utilisateur 
	 * @param idVols : identifiant du vol
	 * @return
	 * @throws SQLException
	 */
	public static boolean ajouterNouveauReservration(String pseudo,String vol) throws SQLException{
		
		/** requete sql qui definie l'action d'insertion d'un nouvelle resevation d'utilisateur a la base de donnée */
		String requete="insert into  "+Table_Base_De_Donnee.table_reservation+" values ('"+pseudo+"','"+vol+"')";
		
		/** etablissement d'une nouvelle connexion a la base de donnée MYSQL*/ 
		Connection connexion = Database.getMySQLConnection();
		
		/** Creation d'un objet de type statement qui s'occupera d'executer la requete */ 
		Statement statement = connexion.createStatement();
		
		/** execution de la requete de type update qui permet d'ajouter une ligne à la table Reservation */ 
		int retour = statement.executeUpdate(requete);
		
		/** destruction de l'objet statement */ 
		statement.close();
		
		/** decconexion de la base de donnée */
		connexion.close();
		
		/** la fonction retournera 0 si l'execution de la fonction n'aboutie a rien */ 
		return retour != 0;
	}
	
	
	/**
	 * 
	 * Service permettant d'annuler une reservation d'un utilisateur à la base de données 
	 * @param pseudo : pseudo de l'utilisateur 
	 * @param idVols : identifiant du vol
	 * @return
	 * @throws SQLException
	 */
	public static boolean annulerReservration(String pseudo,String vol) throws SQLException{
		
		/** requete sql qui definie l'action d'suppression d'une resevation d'un utilisateur a la base de donnée */
		String requete="delete from  "+Table_Base_De_Donnee.table_reservation+" where pseudo = '"+pseudo+"' and vol = '"+vol+"' ";
		
		/** etablissement d'une nouvelle connexion a la base de donnée MYSQL*/ 
		Connection connexion = Database.getMySQLConnection();
		
		/** Creation d'un objet de type statement qui s'occupera d'executer la requete */ 
		Statement statement = connexion.createStatement();
		
		/** execution de la requete de type update qui permet de supprimer la  ligne de la table Reservation */ 
		int retour = statement.executeUpdate(requete);
		
		/** destruction de l'objet statement */ 
		statement.close();
		
		/** decconexion de la base de donnée */
		connexion.close();
		
		/** la fonction retournera 0 si l'execution de la fonction n'aboutie a rien */ 
		return retour != 0;
	}
}
