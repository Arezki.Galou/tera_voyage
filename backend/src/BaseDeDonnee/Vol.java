package BaseDeDonnee;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import outils.Database;
import outils.OutilAPI;
import outils.Table_Base_De_Donnee;

/***
 * 
 * <p>
 * <h3><strong> Class Vol</strong>:</h3>
 * <h4>description</h4>
 * 	qui offre des services lier à la gestions des vols de l'application 
 * <h4>Service Offerts </h4>
 * 		<ul>
 * 			<li> Ajout d'un nouveau vol </li>
 * 		</ul>
 * </p>
 * 
 * @author Kichou Galou Lounis
 *
 */
public class Vol {

	/**
	 * service permettant d'ajouter dans la base de donnée un vols 
	 * @param date :date du vol 
	 * @param aeroportDepart : aeroport de depart pour le vol 
	 * @param aeroportArrivee : destination du vol 
	 * @param nombrePlaces : nombre de plance du vols
	 * @return : TRue si le vol a bien etait ajouter a la base de donnée 
	 * @throws SQLException
	 */
	public static boolean ajouterNouveauVol(String vol ,String date ,String heureVol,String aeroportDepart,String aeroportArrivee,int nombrePlaces,String companie) throws SQLException{

		/** requete sql qui definie l'action d'insertion d'un nouveau vol a la base de donnée */
		String requete="insert into  "+Table_Base_De_Donnee.table_vols+" values ('"+vol+"','"+date+"','"+heureVol+"','"+aeroportDepart+"','"+aeroportArrivee+"',"+nombrePlaces+",'"+companie+"' )";
		
		/** etablissement d'une nouvelle connexion a la base de donnée MYSQL*/ 
		Connection connexion = Database.getMySQLConnection();
		
		/** Creation d'un objet de type statement qui s'occupera d'executer la requete */ 
		Statement statement = connexion.createStatement();

		/** execution de la requete de type update qui permet d'ajouter une ligne à la table vols */ 
		int retour = statement.executeUpdate(requete);
		
		/** destruction de l'objet statement */ 
		statement.close();
		
		/** decconexion de la base de donnée */
		connexion.close();
		
		/** la fonction retournera 0 si l'execution de la fonction n'aboutie a rien */ 
		return retour != 0;
	}
	
	/**
	 * service permettant de recuperer les vols disponible de la base donnée
	 * @return : JSON contennant tout les vols disponible
	 * @throws SQLException
	 * @throws JSONException 
	 */
	public static JSONObject recuperationVol() throws SQLException, JSONException{

		JSONObject reponse = new JSONObject();
		
		/** requete sql qui definie l'action d'insertion d'un nouveau vol a la base de donnée */
		String requete="select vol,dateVol,heureVol,aeroportDepart,aeroportArrivee,nombrePlaces,airline from "+Table_Base_De_Donnee.table_vols+" ";
		
		/** etablissement d'une nouvelle connexion a la base de donnée MYSQL*/ 
		Connection connexion = Database.getMySQLConnection();
		
		/** Creation d'un objet de type statement qui s'occupera d'executer la requete */ 
		Statement statement = connexion.createStatement();
		
		/** execution de la requete de type update qui permet d'ajouter une ligne à la table vols */ 
		ResultSet resultat = statement.executeQuery(requete);
		
		ArrayList< JSONObject> vols = new ArrayList<JSONObject>();
		
		while(resultat.next()) {
			String idvol = resultat.getString("vol");
			
			Connection con = Database.getMySQLConnection();
			
			Statement statement2= con.createStatement();
			
			String requete2="select count(*) from "+Table_Base_De_Donnee.table_reservation+" where vol = '"+idvol+"';";
			
			ResultSet resultat2 = statement2.executeQuery(requete2);
			
			int disponibilites = 0;
			if (resultat2.next()) {
				disponibilites = resultat.getInt("nombrePlaces")-resultat2.getInt("count(*)");
			}
			
			
			JSONObject vol = new JSONObject();
			vol.append("vol", resultat.getString("vol"));
			vol.append("date", resultat.getString("dateVol"));
			vol.append("heure", resultat.getString("heureVol"));
			vol.append("aeroportDepart", resultat.getString("aeroportDepart"));
			vol.append("aeroportArrivee", resultat.getString("aeroportArrivee"));
			vol.append("nombrePlaces", disponibilites);
			vol.append("compagnie", resultat.getString("airline"));
			vols.add(vol);
			
			con.close();
			statement2.close();
			resultat2.close();
		}
		
		reponse.append("id",1);
		reponse.append("vols", vols);
		reponse.append("Message", "vol telecharger ");
		
		/** destruction de l'objet statement */ 
		statement.close();
		
		/** decconexion de la base de donnée */
		connexion.close();
		
		/** la fonction retournera 0 si l'execution de la fonction n'aboutie a rien */ 
		return reponse;
	}
		
	
	/**
	 * service permettant de rechercher un vols dans la base donnée
	 * @param dateVol : date specifiant le jours ou le vol est programmer 
	 * @param aeroportDepart : aeroport du debut du vol 
	 * @param aeroportArrivee : aeroport de destination 
	 * @return : JSON contennant tout les vols disponible
	 * @throws SQLException
	 * @throws JSONException 
	 */
	public static JSONObject rechercheVol(String dateVol,String aeroportDepart ,String aeroportArrivee) throws SQLException, JSONException{

		JSONObject reponse = new JSONObject();
		
		Date dateSql = Date.valueOf(dateVol);
		
		/** requete sql qui definie l'action d'insertion d'un nouveau vol a la base de donnée */
		String requete="select vol,dateVol,heureVol,aeroportDepart,aeroportArrivee,nombrePlaces,airline from "+Table_Base_De_Donnee.table_vols+" where dateVol = '"+dateSql+"' and aeroportDepart ='"+aeroportDepart+"' and aeroportArrivee = '"+aeroportArrivee+"'";    
		
		/** etablissement d'une nouvelle connexion a la base de donnée MYSQL*/ 
		Connection connexion = Database.getMySQLConnection();
		
		/** Creation d'un objet de type statement qui s'occupera d'executer la requete */ 
		Statement statement = connexion.createStatement();
		
		/** execution de la requete de type update qui permet d'ajouter une ligne à la table vols */ 
		ResultSet resultat = statement.executeQuery(requete);
		
		ArrayList< JSONObject> vols = new ArrayList<JSONObject>();
		
		while(resultat.next()) {
			String idvol = resultat.getString("vol");
			
			Connection con = Database.getMySQLConnection();
			
			Statement statement2= con.createStatement();
			
			String requete2="select count(*) from "+Table_Base_De_Donnee.table_reservation+" where vol = '"+idvol+"';";
			
			ResultSet resultat2 = statement2.executeQuery(requete2);
			
			int disponibilites = 0;
			if (resultat2.next()) {
				disponibilites = resultat.getInt("nombrePlaces")-resultat2.getInt("count(*)");
			}
			
			
			JSONObject vol = new JSONObject();
			vol.append("vol", resultat.getString("vol"));
			vol.append("date", resultat.getString("dateVol"));
			vol.append("heure", resultat.getString("heureVol"));
			vol.append("aeroportDepart", resultat.getString("aeroportDepart"));
			vol.append("aeroportArrivee", resultat.getString("aeroportArrivee"));
			vol.append("nombrePlaces", disponibilites);
			vol.append("compagnie", resultat.getString("airline"));
			vols.add(vol);
			
			con.close();
			statement2.close();
			resultat2.close();
		}
		
		reponse.append("id",1);
		reponse.append("vols", vols);
		reponse.append("Message", "vol telecharger ");
		
		/** destruction de l'objet statement */ 
		statement.close();
		
		/** decconexion de la base de donnée */
		connexion.close();
		
		/** la fonction retournera 0 si l'execution de la fonction n'aboutie a rien */ 
		return reponse;
	}
	
	
	
}
