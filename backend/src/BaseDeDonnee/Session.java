package BaseDeDonnee;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import outils.Database;
import outils.Table_Base_De_Donnee;

/**
 * <p>
 * <h3><strong> Class Session</strong>:</h3>
 * <h4>description</h4>
 * 	qui offre des services lier à la gestions des Session des utilisateur de l'application 
 * <h4>Service Offerts </h4>
 * 		<ul>
 * 			<li> ouverture d'une session à un utilisateur donner </li>
 * 			<li> fermeture d'une session à un utilisateur   </li>
 * 			<li> mise à jour de la clef de sessions d'un utilisateur  </li>
 * 		</ul>
 * </p>
 * @author Kichou Galou Lounis
 *
 */
public class Session {
	
	/**
	 * Service permettant de lancer une session pour un utilisateur donner
	 * @param pseudo : pseudo de l'utilisateur de la sessions
	 * @param clef : clef de session
	 * @return : True si la session a bien etait lancer et False sinon
	 * @throws SQLException
	 */
	public static boolean lancerSession(String pseudo , String  clef) throws SQLException {
		/** requete sql qui definie l'action d'ouverture d'une session de utilisateur a la base de donnée */
		String requete="insert into  "+Table_Base_De_Donnee.table_session+"(pseudo,clef) values ('"+pseudo+"','"+clef+"');";
		
		/** etablissement d'une nouvelle connexion a la base de donnée MYSQL*/ 
		Connection connexion = Database.getMySQLConnection();
		
		/** Creation d'un objet de type statement qui s'occupera d'executer la requete */ 
		Statement statement = connexion.createStatement();
		
		/** execution de la requete de type update qui permet d'ajouter une ligne à la table utilisateur */ 
		int retour = statement.executeUpdate(requete);
		
		/** destruction de l'objet statement */ 
		statement.close();
		
		/** decconexion de la base de donnée */
		connexion.close();
		
		/** la fonction retournera 0 si l'execution de la fonction n'aboutie a rien */ 
		return retour != 0;
	}
	
	/**
	 * Service permettant de fermer une session pour un utilisateur donner
	 * @param pseudo : pseudo de l'utilisateur de la sessions
	 * @return : True si la session a bien etait fermer et False sinon
	 * @throws SQLException
	 */
	public static boolean fermerSession(String clef) throws SQLException {
		/** requete sql qui definie l'action d'ouverture d'une session de utilisateur a la base de donnée */
		String requete="delete from "+Table_Base_De_Donnee.table_session+" where clef = '"+clef+"';";
		
		/** etablissement d'une nouvelle connexion a la base de donnée MYSQL*/ 
		Connection connexion = Database.getMySQLConnection();
		
		/** Creation d'un objet de type statement qui s'occupera d'executer la requete */ 
		Statement statement = connexion.createStatement();
		
		/** execution de la requete de type update qui permet d'ajouter une ligne à la table utilisateur */ 
		int retour = statement.executeUpdate(requete);
		
		/** destruction de l'objet statement */ 
		statement.close();
		
		/** decconexion de la base de donnée */
		connexion.close();
		
		/** la fonction retournera 0 si l'execution de la fonction n'aboutie a rien */ 
		return retour != 0;
	}
	
	
	/**
	 * Service permettant de mettre à jour la clef de session pour un utilisateur donner
	 * @param pseudo : pseudo de l'utilisateur de la sessions
	 * @param clef : nouvelle clef de session
	 * @return : True si la session a bien etait lancer et False sinon
	 * @throws SQLException
	 */
	public static boolean nouvelleClefSession(String pseudo , String  nouvelleclef) throws SQLException {
		/** requete sql qui definie l'action d'ouverture d'une session de utilisateur a la base de donnée */
		String requete="update "+Table_Base_De_Donnee.table_session+" set clef = '"+nouvelleclef+"' where pseudo = '"+pseudo+"';";
		
		/** etablissement d'une nouvelle connexion a la base de donnée MYSQL*/ 
		Connection connexion = Database.getMySQLConnection();
		
		/** Creation d'un objet de type statement qui s'occupera d'executer la requete */ 
		Statement statement = connexion.createStatement();
		
		/** execution de la requete de type update qui permet d'ajouter une ligne à la table utilisateur */ 
		int retour = statement.executeUpdate(requete);
		
		/** destruction de l'objet statement */ 
		statement.close();
		
		/** decconexion de la base de donnée */
		connexion.close();
		
		/** la fonction retournera 0 si l'execution de la fonction n'aboutie a rien */ 
		return retour != 0;
	}
	
	
}
