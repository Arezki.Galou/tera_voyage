package BaseDeDonnee;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;


import org.json.JSONException;
import org.json.JSONObject;

import outils.Database;
import outils.Table_Base_De_Donnee;
/**
 * <p>
 * <h3><strong> Class Utilisateur</strong>:</h3>
 * <h4>description</h4>
 * 	qui offre des services lier à la gestions des utilisateurs de l'application 
 * <h4>Service Offerts </h4>
 * 		<ul>
 * 			<li> Ajout d'un nouvelle utilisateur </li>
 * 			<li> Suppression d'un nouvelle utilisateur </li>
 * 			<li> Modification du mot de passe d'un utilisateur </li>
 * 		</ul>
 * </p>
 * 
 * @author Kichou Galou Lounis 
 *
 */
public class Utilisateur {
	/**
	 * Service permettant d'ajouter un nouvelle utilisateur à la base de données et ainsi de creer un nouveau compte 
	 * @param nom : nom du nouvelle utilisateur  ajouter 
	 * @param prenom :prenom du nouvelle utilisateur  ajouter 
	 * @param pseudo :pseudo de utilisateur qui permetteran la connexion a sa session
	 * @param motDePasse : mot de passe pour protéger la session de l'utilisateur 
	 * @param age : age de l'utilisateur 
	 * @throws SQLException : possibilité de lever une exeption de type SQLException 
	 * @return : True si l'utilisateur a bien etait ajouter a la base de donnée 
	 */
	public static boolean ajouterNouveauUtilisateur(String nom,String prenom,String pseudo,String email,String motDePasse,int age, String telephone) throws SQLException{
		
		/** requete sql qui definie l'action d'insertion d'un nouvelle utilisateur a la base de donnée */
		String requete="insert into  "+Table_Base_De_Donnee.table_utilisateur+" values ('"+nom+"','"+prenom+"','"+pseudo+"','"+email+"','"+motDePasse+"',"+age+",'"+telephone+"' )";
		
		/** etablissement d'une nouvelle connexion a la base de donnée MYSQL*/ 
		Connection connexion = Database.getMySQLConnection();
		
		/** Creation d'un objet de type statement qui s'occupera d'executer la requete */ 
		Statement statement = connexion.createStatement();
		
		/** execution de la requete de type update qui permet d'ajouter une ligne à la table utilisateur */ 
		int retour = statement.executeUpdate(requete);
		
		/** destruction de l'objet statement */ 
		statement.close();
		
		/** decconexion de la base de donnée */
		connexion.close();
		
		/** la fonction retournera 0 si l'execution de la fonction n'aboutie a rien */ 
		return retour != 0;
	}
	/**
	 * Service permettant de supprimer un compte utilisateur de la base de donnée 
	 * @param pseudo : pseudo du compte a supprimer 
	 * @throws SQLException
	 * @return : True si l'utilisateur a bien etait supprimer de la base de donnée s
	 */
	public static boolean supprimerUtilisateur(String pseudo) throws SQLException {
		
		/**  requete sql qui definie l'action de suppression d'un utilisateur de la base de donnée */
		String requete= "DELETE FROM "+Table_Base_De_Donnee.table_utilisateur+" WHERE pseudo = '"+pseudo+"';";
		
		/** etablissement d'une nouvelle connexion a la base de donnée MYSQL*/ 
		Connection connexion = Database.getMySQLConnection();
		
		/** Creation d'un objet de type statement qui s'occupera d'executer la requete */ 
		Statement statement = connexion.createStatement();
		
		/** execution de la requete de type update qui permet de supprimer une ligne à la table utilisateur */ 
		int retour = statement.executeUpdate(requete);
		
		/** destruction de l'objet statement */ 
		statement.close();
		
		/** decconexion de la base de donnée */
		connexion.close();
		
		return retour != 0;
	}
	
	/**
	 * Service Permettant de modifier le mot de passe d'un utilisateur 
	 * @param pseudo : pseudo de l'utilisateur 
	 * @param nouveauMotDePasse : nouveau mot de passe 
	 * @throws SQLException
	 * @return True is une ligen de la base de donnée a etait modifier et False sinon 
	 */
	public static boolean modificationModeDePasseUtilisateur(String pseudo, String nouveauMotDePasse) throws SQLException {
		
		/**  requete sql qui definie l'action de modification du mot de pass d'un utilisateur dans la base de donnée */
		String requete= "UPDATE "+Table_Base_De_Donnee.table_utilisateur+" Set motDePasse = '"+nouveauMotDePasse+"' where pseudo='"+pseudo+"';";
		
		/** etablissement d'une nouvelle connexion a la base de donnée MYSQL*/ 
		Connection connexion = Database.getMySQLConnection();
		
		/** Creation d'un objet de type statement qui s'occupera d'executer la requete */ 
		Statement statement = connexion.createStatement();
		
		/** execution de la requete de type update qui permet de supprimer une ligne à la table utilisateur */ 
		int retour = statement.executeUpdate(requete);
		
		/** destruction de l'objet statement */ 
		statement.close();
		
		/** decconexion de la base de donnée */
		connexion.close();
		
		/** le service retourne ne retournera pas 0 si l'action a bien etait executer */
		return retour != 0;
	}
	
	/**
	 * Service Permettant de recuperer toutes les informations d'un utilisateur 
	 * @param pseudo : pseudo de l'utilisateur 
	 * @throws SQLException
	 * @return JSON avec tout les information de l'utilisateur
	 * @throws JSONException 
	 */
	public static JSONObject recuperationProfile(String pseudo) throws SQLException, JSONException {
		
		JSONObject reponse = new JSONObject();
		
		/**  requete sql qui definie l'action de recuperation des inforamtion du profile*/
		String requete= "SELECT nom,prenom,age,email,pseudo,telephone from "+Table_Base_De_Donnee.table_utilisateur+" where pseudo = '"+pseudo+"';";
		
		/**  requete sql qui definie l'action de recuperation des reservations de l'utilisateur */
		String requete2= "SELECT v.vol,v.dateVol,v.heureVol,v.aeroportDepart,v.aeroportArrivee,v.airline from "+Table_Base_De_Donnee.table_vols+" v ,"+Table_Base_De_Donnee.table_reservation+" r, "+Table_Base_De_Donnee.table_utilisateur+" u where u.pseudo = '"+pseudo+"' and r.vol=v.vol and u.pseudo=r.pseudo";
		
		/** etablissement d'une nouvelle connexion a la base de donnée MYSQL*/ 
		Connection connexion = Database.getMySQLConnection();
		
		/** Creation d'un objet de type statement qui s'occupera d'executer la requete */ 
		Statement statement = connexion.createStatement();
		
		/** Creation d'un objet de type statement qui s'occupera d'executer la requete2 */ 
		Statement statement2 = connexion.createStatement();
		
		/** execution de la requete de type Query qui permet de recuperer la ligne de  la table utilisateur  qui correspond au pseudo*/ 
		ResultSet resultat = statement.executeQuery(requete);
		
		ResultSet resulat2 = statement2.executeQuery(requete2);
		if(resultat.next()) {
			reponse.append("id",1);
			reponse.append("nom", resultat.getString("nom"));
			reponse.append("prenom", resultat.getString("prenom"));
			reponse.append("age", resultat.getInt("age"));
			reponse.append("email", resultat.getString("email"));
			reponse.append("telephone", resultat.getString("telephone"));
			reponse.append("pseudo", resultat.getString("pseudo"));

			/**recuperations des reservations de l'utilisateur */
		
			ArrayList<JSONObject> retour = new ArrayList<JSONObject>();
			while (resulat2.next()) {
				JSONObject reservations = new JSONObject();
				reservations.append("vol",resulat2.getString("vol"));
				reservations.append("date",resulat2.getString("dateVol"));
				reservations.append("heure",resulat2.getString("heureVol"));
				reservations.append("aeroportDepart",resulat2.getString("aeroportDepart"));
				reservations.append("aeroportArrivee",resulat2.getString("aeroportArrivee"));
				reservations.append("compagnie",resulat2.getString("airline"));
				retour.add(reservations);
			}
			reponse.append("reservations", retour);
		}else {
			reponse.append("id", 0);
			reponse.accumulate("Cause", "pseudo inexistant");
		}
		
		/**destruction de lobjet resultSet*/
		resultat.close();
		resulat2.close();
		/** destruction de l'objet statement */ 
		statement.close();
		statement2.close();
		/** decconexion de la base de donnée */
		connexion.close();
		
		/** le service retourne ne retournera pas 0 si l'action a bien etait executer */
		return reponse;
	}
	
	
}
