package Service;

import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import outils.OutilAPI;
import outils.OutilSession;

/**
 *<p>
 * <h3><strong> Class Vol</strong>:</h3>
 * <h4>description</h4>
 * 	qui offre des services lier à la gestions des Vols de l'application avec les actions complete
 * <h4>actions  </h4>
 * 		<ul>
 * 			<li> recuperation des vols de l'api et l'ajout dans la base de donnée </li>
 * 		</ul>
 * </p>
 * @author Kichou Galou Lounis
 *
 */
public class Vol {

	public static void ajouterVol(String vol,String date,String heureVol,String aeroportDepart,String aeroportArrivee,int nombrePlaces,String  compagnie) throws Exception {
		
		if (date == null || aeroportArrivee == null || aeroportDepart == null || nombrePlaces < 0) {
			throw new Exception("argument invalid");
		}
		if (outils.OutilsVol.testExistanceVol(vol)) return;

		BaseDeDonnee.Vol.ajouterNouveauVol(vol,date,heureVol,aeroportDepart, aeroportArrivee, nombrePlaces,compagnie);
	}
	/**
	 * Service permettatn de recuperer tout les vols diponibles 
	 * @param clef : clef de sessions
	 * @return JSON avec tout les vols disponible
	 * @throws JSONException 
	 * @throws SQLException 
	 */
	public static JSONObject recuperationVols(String clef) throws SQLException, JSONException  {
		JSONObject reponse = new JSONObject();
		if (clef== null ) {
			reponse.append("id", 0);
			reponse.append("erreur", 100);
			reponse.append("Cause", "arguments invalide ");
		}else {
			/** verification si la session est lancer */
			if (!OutilSession.existanceClef(clef)) {
				reponse.append("id", 0);
				reponse.append("erreur", 300);
				reponse.append("Cause"," vous n'etes pas connecter / recconnectez vous S'il vous plait ");
			}else {
				/** verification si la clef est valide et que y'a pas un temps d'inactivité de 30 mins */
				if (!OutilSession.validiteClef(clef)) {
					reponse.append("id", 0);
					reponse.append("erreur", 400);
					reponse.append("Cause"," temps de non activité depassant 30 mins reconnecter vous ");
				}else {
					reponse = BaseDeDonnee.Vol.recuperationVol();
				}
			}
		
		}
		return reponse;
	}

	/**
	 * Service permettatn de recuperer tout les vols diponibles 
	 * @param clef : clef de sessions
	 * @return JSON avec tout les vols disponible
	 * @throws JSONException 
	 * @throws SQLException 
	 */
	public static JSONObject rechercheVols(String clef,String dateVol,String aeroportDepart,String aeroportArrivee) throws SQLException, JSONException  {
		JSONObject reponse = new JSONObject();
		if (clef== null || dateVol == null || aeroportDepart == null || aeroportArrivee == null  ) {
			reponse.append("id", 0);
			reponse.append("erreur", 100);
			reponse.append("Cause", "arguments invalide ");
		}else {
			/** verification si la session est lancer */
			if (!OutilSession.existanceClef(clef)) {
				reponse.append("id", 0);
				reponse.append("erreur", 300);
				reponse.append("Cause"," vous n'etes pas connecter / recconnectez vous S'il vous plait ");
			}else {
				/** verification si la clef est valide et que y'a pas un temps d'inactivité de 30 mins */
				if (!OutilSession.validiteClef(clef)) {
					reponse.append("id", 0);
					reponse.append("erreur", 400);
					reponse.append("Cause"," temps de non activité depassant 30 mins reconnecter vous ");
				}else {
					reponse = BaseDeDonnee.Vol.rechercheVol(dateVol, aeroportDepart, aeroportArrivee);
				}
			}
		
		}
		return reponse;
	}

	public static void recuperationVolsAPI() throws Exception {
		JSONObject obj=OutilAPI.Methode1();
		JSONArray jarr=obj.getJSONArray("data");
		System.out.println(jarr.get(0)+"\n");
		System.out.println("nombre de vol dans l'api "+jarr.length());
		for(int i=0 ;i<jarr.length();i++) {
			obj= (JSONObject) jarr.get(i);
			JSONObject o = new JSONObject(obj.get("airline").toString());
			String companie = o.getString("name");
			 o = new JSONObject(obj.get("flight").toString());
			 String id_vol=o.getString("iata");
			 o=new JSONObject(obj.get("arrival").toString());
			 String airport_arrive=o.getString("airport");
			 o=new JSONObject(obj.get("departure").toString());
			 String airport_depart=o.getString("airport");
			 String time_date=o.getString("estimated");
			 int nbplaces=(int) (90 + (Math.random() * (220 - 90)));
			 try {
				 	ajouterVol(id_vol,OutilAPI.parserDate(time_date), OutilAPI.parserHeure(time_date), airport_depart, airport_arrive, nbplaces, companie);
					System.out.println("Ajout réussi");
				} catch (SQLException e) {
					System.out.println("nom aeroport avec symbole speciale ");
				}
		}
	}
}	
	
	

