package Service;

import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;

import outils.OutilSession;
import outils.OutilsUtilisateur;

/**
 * <p>
 * <h3><strong> Class Utilisateur</strong>:</h3>
 * <h4>description</h4>
 * 	qui offre des services lier à la gestions des Utilisateur des utilisateur de l'application avec les actiosn compelte
 * <h4>actions  </h4>
 * 		<ul>
 * 			<li> creation d'un compte utilisateur avec verification des arguments et unicité du pseudo </li>
 * 			<li> suppresion d'un compte avec verification de la validité de la clef de session , existance de la clef </li>
 * 			<li> modification du mot de passe d'un compte avec pris en compte des arguments et verification de la session de l'utilisateur et l'existance du profile </li>
 * 		</ul>
 * </p>
 * @author Kichou Galou Lounis
 *
 */
public class Utilisateur {

	/**
	 * Service permettant l'ajout d'un nouvelle utlisateur a l'application avec toutes les verifiaction possible  avant l'ajout
	 * @param nom: nom de l'utilisateur a ajouter 
	 * @param prenom : prenom de l'utilisateur a ajouter 
	 * @param pseudo : pseudo de l'utilisateur a ajouter 
	 * @param motDePasse : motDePasse de l'utilisateur a ajouter 
	 * @param age: age de l'utilisateur a ajouter 
	 * @return Object JSON avec la clef de session de l'utilisateur sinon erreur 
	 * @throws JSONException
	 * @throws SQLException
	 */
	public static JSONObject ajouterUtilisateur(String nom,String prenom, String pseudo ,String email,String motDePasse,int age,String telephone) throws JSONException, SQLException {
		/** objet JSOn a retouner */
		JSONObject reponse = new JSONObject();
		
		/** test si un des arguments n'ai pas valable */
		if ( nom == null || prenom == null || pseudo == null || email == null ||motDePasse == null || age < 18 || telephone == null) {
			/** ajout de l'erreur au message du JSON*/
			reponse.append("id", 0);
			reponse.append("erreur", 100);
			reponse.append("Cause", "arguments invalide ");
		}else {
			/** verification si l'utilisateur existe deja pour pouvoir l'ajouter */
			if ( OutilsUtilisateur.testExistanceUtilisateur(pseudo)) {
				/** renvoie d'une erreure a l'utilisateur */
				reponse.append("id", 0);
				reponse.append("erreur",200);
				reponse.append("Cause", "Pseudo déja utiliser");
			}else {
				/** ajouter  l'utilisateur a la base de donnée */
				BaseDeDonnee.Utilisateur.ajouterNouveauUtilisateur(nom, prenom, pseudo,email, motDePasse, age, telephone);
				
				/**lancer une session pour l'utilisateur*/
				reponse.append("id", 1);
				reponse = Service.Session.demarrerSession(pseudo,motDePasse);
				
			}
			
		}
		return reponse;
	}
	
	/**
	 * service permettant a un utilisateur de supprimer son compte 
	 * @param clef : Clef de session de l'utilisateur 
	 * @return : objet JSON 
	 * @throws JSONException
	 * @throws SQLException
	 */
	public static JSONObject supprimerUtilisateur(String clef) throws JSONException, SQLException {
		/** objet JSON pour la reponse */
		JSONObject reponse = new JSONObject();
		
		/** verification que la clef est instancié*/
		if (clef == null ) {
			/** si la clef est null erreur 100 manque d'argument */
			reponse.append("id", 0);
			reponse.append("erreur", 100);
			reponse.append("Cause", "clef invalid ");
		}else {
			/** verification si la session est lancer */
			if (!OutilSession.existanceClef(clef)) {
				reponse.append("id", 0);
				reponse.append("erreur", 300);
				reponse.append("Cause"," vous n'etes pas connecter / recconnectez vous S'il vous plait ");
			}else {
				/** verification si la clef est valide et que y'a pas un temps d'inactivité de 30 mins */
				if (!OutilSession.validiteClef(clef)) {
					reponse.append("id", 0);
					reponse.append("erreur", 400);
					reponse.append("Cause"," temps de non activité depassant 30 mins reconnecter vous ");
				}else {
					/** recuperation du pseudo de l'utilisateur  */
					String pseudo = OutilSession.recuperationPseudo(clef);  
					
					/** suppresion de l'utilisateur en cascade ce qui supprime aussi sa session*/
					BaseDeDonnee.Utilisateur.supprimerUtilisateur(pseudo);
					
					/** reponse a l'utilisateur avec message de confirmation de suppression */
					reponse.append("id", 1);
					reponse.append("Message", "utilusateur supprimer avec succées au revoir ");
				}
			}
		}
		return reponse;
	}
	
	/**
	 * Service permettant de modifier le mot de passe d'un tulisateur 
	 * @param clef : clef de session de l'utilisateur
	 * @param motDePasse : nouveau mot de passe de l'tulisateur 
	 * @return objet JSON  avec la confirmation de la modification
	 * @throws JSONException 
	 * @throws SQLException 
	 */
	public static JSONObject modifierMotDePasseUtilisateur(String clef ,String MotDePasse,String nouveauMotDePasse) throws JSONException, SQLException{
		
		/** objet JSONclef a renvoyez comme reponse  au client */
		JSONObject reponse = new JSONObject();
		
		/** argument invalide ( clef vide ou mot de passe vide) */
		if (clef == null || nouveauMotDePasse == null || MotDePasse == null) {
			reponse.append("id", 0);
			reponse.append("erreur", 100);
			reponse.append("Cause", "argument invalide ");
		
		}else {
			
			/** l'utilisateur n'ai pas connecter*/
			if (!OutilSession.existanceClef(clef)) {
				reponse.append("id", 0);
				reponse.append("erreur", 300);
				reponse.append("Cause","clef invalide ");
			
			}else {
				
				/** l(utilsateur a une clef expirer ( inactivité 30 min )*/
				if(!OutilSession.validiteClef(clef)) {
					reponse.append("id", 0);
					reponse.append("erreur", 400);
					reponse.append("Cause", " temps de non activité depassant 30 mins reconnecter vous ");
				
				}else {
					/**recuperation du pseudo de l'utilisateur*/
					String pseudo = OutilSession.recuperationPseudo(clef);
					if (OutilsUtilisateur.verificationMotDePasse(pseudo, MotDePasse)) {
						reponse.append("id", 0);
						reponse.append("erreur", 400);
						reponse.append("Cause", " mot de passe courant incorrecte ");
					}else {
					
						/**mise a jour du mot de passe dans la base de donnée */
						BaseDeDonnee.Utilisateur.modificationModeDePasseUtilisateur(pseudo, nouveauMotDePasse);
						
						/** message de confirmation*/
						reponse.append("id", 1);
						reponse.append("Message", "Mot de passe Mise a Jour ;) ");
				
					}
				}
			}
		}
		return reponse;

	}
	
	
	/**
	 * service permettant a un utilisateur de supprimer son compte 
	 * @param clef : Clef de session de l'utilisateur 
	 * @return : objet JSON 
	 * @throws JSONException
	 * @throws SQLException
	 */
	public static JSONObject profileUtilisateur(String clef) throws JSONException, SQLException {
		/** objet JSON pour la reponse */
		JSONObject reponse = new JSONObject();
		
		/** verification que la clef est instancié*/
		if (clef == null ) {
			/** si la clef est null erreur 100 manque d'argument */
			reponse.append("id", 0);
			reponse.append("erreur", 100);
			reponse.append("Cause", "clef invalid ");
		}else {
			/** verification si la session est lancer */
			if (!OutilSession.existanceClef(clef)) {
				reponse.append("id", 0);
				reponse.append("erreur", 300);
				reponse.append("Cause"," vous n'etes pas connecter / recconnectez vous S'il vous plait ");
			}else {
				/** verification si la clef est valide et que y'a pas un temps d'inactivité de 30 mins */
				if (!OutilSession.validiteClef(clef)) {
					reponse.append("id", 0);
					reponse.append("erreur", 400);
					reponse.append("Cause"," temps de non activité depassant 30 mins reconnecter vous ");
				}else {
					/** recuperation du pseudo de l'utilisateur  */
					String pseudo = OutilSession.recuperationPseudo(clef);  
					
					/** recuperation des inforamtions sur l'utilisateur */
					reponse = BaseDeDonnee.Utilisateur.recuperationProfile(pseudo);
					
				}
			}
		}
		return reponse;
	}
	
	
}
