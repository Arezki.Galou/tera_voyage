package test_unitaire.base_de_donnee;

import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.text.ParseException;

import org.json.JSONException;
import org.junit.Test;

import BaseDeDonnee.Vol;
import outils.OutilsVol;

public class Vol_test {

	String date ="1997-12-22";
	String aeroportDepart = "paris charle de gaulle";
	String aeroportArrivee = "alger";
	int nombrePlaces = 1;
	
	@Test
	public void ajouterVol() throws SQLException, ParseException, JSONException {
		BaseDeDonnee.Vol.ajouterNouveauVol(OutilsVol.generationVol(),date,date, aeroportDepart, aeroportArrivee, nombrePlaces,new String("GALOU_Airlines"));
		assertTrue(OutilsVol.testExistanceVol("O4D3U"));
		assertTrue(OutilsVol.testDisponibiliteVol("O4D3U"));
		System.out.println(Vol.recuperationVol());
	}
	
	@Test
	public void recherchervol() {
		String dateVol = "1997-12-22";
		try {
			System.out.println("les vols ");
			System.out.println(Vol.rechercheVol(dateVol, aeroportDepart, aeroportArrivee));
		} catch (SQLException | JSONException e) {
			e.printStackTrace();
		}
		
	}
	
}
