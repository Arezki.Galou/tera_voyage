package outils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**
 * <p>
 * <h3><strong> Class OutilsUtilisateur</strong>:</h3>
 * <h4>description</h4>
 * 	qui offre des services liers au verification s des informations des utilisateurs 
 * <h4>Service Offerts </h4>
 * 		<ul>
 * 			<li> verification du mot de passe s'il est correct </li>
 * 			<li> verification si un utilisateur existe dejas dans la base de donnée </li>
 * 		</ul>
 * </p>
 * 
 * @author Kichou Galou et Lounis
 *
 */
public class OutilsUtilisateur {

	/**
	 * fonction qui est la validité du mot passe lors de la connexion 
	 * @param pseudo : pseudo du compte a tester 
	 * @param MotDePasse : mot de passe a tester 
	 * @return : True si  motDePasse correspond au mot de passe enreigstrer la base de donnée et FAlse sinon
	 * @throws SQLException
	 */
	public static boolean verificationMotDePasse(String pseudo, String MotDePasse) throws SQLException {
		/** requete sql qui definie l'action verification du mot de passe */
		String requete="select motDePasse from "+Table_Base_De_Donnee.table_utilisateur+" where pseudo = '"+pseudo+"';";
		
		/** etablissement d'une nouvelle connexion a la base de donnée MYSQL*/ 
		Connection connexion = Database.getMySQLConnection();
		
		/** Creation d'un objet de type statement qui s'occupera d'executer la requete */ 
		Statement statement = connexion.createStatement();
		

		/** execution de la requete de type Query qui permet de recuperer les lignes de la table utilisateur */ 
		ResultSet resultat = statement.executeQuery(requete);
		
		boolean retour=false;
		
		while (resultat.next()) {
			retour = resultat.getString("motDePasse").equals(MotDePasse);
		}
		/** fermeture du resultat*/ 
		resultat.close();
		
		/** destruction de l'objet statement */ 
		statement.close();
		
		/** decconexion de la base de donnée */
		connexion.close();
		
		/** la fonction retournera 0 si l'execution de la fonction n'aboutie a rien */ 
		return retour ;
	}
	
	/**
	 * fonction qui teste l'existance d'un utilisateur dans la base de donnée
	 * @param pseudo : pseudo du compte utilisateur 
	 * @return : True si le pseudo existe et Faslse sinon 
	 * @throws SQLException
	 */
	public static boolean testExistanceUtilisateur( String pseudo ) throws SQLException {
		/** requete sql qui definie l'action de test d'existance d'un utilisateur */
		String requete="select pseudo from "+Table_Base_De_Donnee.table_utilisateur+" where pseudo = '"+pseudo+"';";
	
		/** etablissement d'une nouvelle connexion a la base de donnée MYSQL*/ 
		Connection connexion = Database.getMySQLConnection();
		
		/** Creation d'un objet de type statement qui s'occupera d'executer la requete */ 
		Statement statement = connexion.createStatement();
		
		/** execution de la requete de type Query qui recuperer les lignes ou le pseudo est egalea  celui donner ne  paramettre */ 
		ResultSet resultat = statement.executeQuery(requete);
		
		/** verification si le pseudo existe deja ou pas */;
		boolean retour = resultat.next();
		
		/** fermeture du resultat*/ 
		resultat.close();
		
		/** destruction de l'objet statement */ 
		statement.close();
		
		/** decconexion de la base de donnée */
		connexion.close();
		
		/** la fonction retournera 0 si l'execution de la fonction n'aboutie a rien */ 
		return retour ;
	
	}
	
}
