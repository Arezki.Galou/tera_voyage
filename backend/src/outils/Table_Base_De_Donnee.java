package outils;

public class Table_Base_De_Donnee {
	
	/** table_utilisateur : nom de la table dans la base de donnée MySQL qui sauvegarde (stocks) les utilisateur   
		description utilisateur (nom,prenom,age,pseudo,motdepasse,email,telephone)**/
	public static String table_utilisateur ="utilisateur";
	
	/** table_session : nom de la table dans la base de donnée MySQL qui sauvegarde les sessions des utilisateur
	 * description Session(pseudo,clef)
	 *  */
	public static String table_session = "session";
	
	/** table_vols : nom de la table dans la base de donnée MySQL qui sauvegarde les vols 
	 * description Vols(id,datevols,aeroportDepart,aeroportArriveee,nombrePlaces)
	 * */
	public static String table_vols = "Vols";
	
	/** table_reservations : nom de la table dans la base de donnée MySQL qui sauvegarde les reservations des utilisateur
	 * descritpion Rerservation(pseudo,idVols)
	 *  */
	public static String table_reservation = "Reservation";
	
}
