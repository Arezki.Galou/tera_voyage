-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 10, 2020 at 08:00 PM
-- Server version: 10.3.22-MariaDB-1
-- PHP Version: 7.3.15-3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Kichou_Galou_Lounis`
--

-- --------------------------------------------------------

--
-- Table structure for table `Reservation`
--

CREATE TABLE `Reservation` (
  `pseudo` varchar(32) NOT NULL,
  `vol` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Reservation`
--

INSERT INTO `Reservation` (`pseudo`, `vol`) VALUES
('kiki', 'CNS181'),
('kiki', 'KO43'),
('kiki', 'NDL901'),
('kiki', 'QR837'),
('kiki', 'THU880'),
('kiki', 'VA5662'),
('kiki', 'VN416'),
('yanis.kichou', 'LX9001'),
('yanis.kichou', 'OU5815');

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `pseudo` varchar(32) NOT NULL,
  `clef` varchar(32) NOT NULL,
  `temps` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`pseudo`, `clef`, `temps`) VALUES
('kiki', 'CaXNDsJkDDQqavKhASLnWb4aur6QayRM', '2020-06-10 17:45:54'),
('kiki', 'kwCQeIiUYMX7unTzfP1Xtjma19ClQ9xT', '2020-06-10 17:48:19'),
('kiki', 'ojDjMQSeg06Uhb7fV73FxQ7IZ4cnfiEW', '2020-06-10 17:38:37'),
('yanis.kichou', 'tw9KPdatIcg908wKoo1k4MZUQ5nwr9Og', '2020-06-10 02:46:14');

-- --------------------------------------------------------

--
-- Table structure for table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `nom` varchar(32) NOT NULL,
  `prenom` varchar(32) NOT NULL,
  `pseudo` varchar(32) NOT NULL,
  `email` varchar(32) NOT NULL,
  `motDePasse` blob NOT NULL,
  `age` int(11) NOT NULL,
  `telephone` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `utilisateur`
--

INSERT INTO `utilisateur` (`nom`, `prenom`, `pseudo`, `email`, `motDePasse`, `age`, `telephone`) VALUES
('GALOU', 'Arezki', 'kiki', 'galouarezki@gmail.com', 0x6b696b69, 24, '0767879113'),
('Kichou', 'yanis', 'yanis.kichou', 'yanis.kichou1997@hotmail.com', 0x59616e6973313939372f, 23, '0667451288');

-- --------------------------------------------------------

--
-- Table structure for table `Vols`
--

CREATE TABLE `Vols` (
  `vol` varchar(50) NOT NULL,
  `dateVol` varchar(20) NOT NULL,
  `heureVol` varchar(100) NOT NULL,
  `aeroportDepart` varchar(100) NOT NULL,
  `aeroportArrivee` varchar(100) NOT NULL,
  `nombrePlaces` int(100) NOT NULL,
  `airline` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Vols`
--

INSERT INTO `Vols` (`vol`, `dateVol`, `heureVol`, `aeroportDepart`, `aeroportArrivee`, `nombrePlaces`, `airline`) VALUES
('0Q282', '2020-06-10', '13:00:00', 'Rouyn-Noranda', 'Nemiscau', 153, 'Hydro - Quebec'),
('0Q332', '2020-06-10', '13:00:00', 'Québec', 'Havre St Pierre', 154, 'Hydro - Quebec'),
('1I374', '2020-06-10', '10:18:00', 'Ryan', 'St. Petersburg-Clearwater International', 157, 'NetJets Aviation'),
('1I407', '2020-06-10', '10:30:00', 'Sky Harbor International', 'San Diego International Airport', 182, 'NetJets Aviation'),
('1I609', '2020-06-10', '10:00:00', 'Dallas/Fort Worth International', 'Decatur Airport', 134, 'NetJets Aviation'),
('3H782', '2020-06-10', '12:30:00', 'Pierre Elliott Trudeau International', 'Kuujjuaq', 93, 'Air Inuit'),
('5A2602', '2020-06-10', '09:30:00', 'Glacier Park International', 'Billings', 205, 'Alpine Air Express'),
('5T228', '2020-06-10', '10:40:00', 'Yellowknife', 'Kugluktuk', 149, 'Canadian North'),
('5Y3716', '2020-06-10', '09:04:00', 'null', 'Honolulu International', 98, 'Atlas Air'),
('5Y6902', '2020-06-10', '11:00:00', 'Fort Worth Alliance', 'Portland International', 147, 'Atlas Air'),
('5Y8500', '2020-06-10', '11:50:00', 'Cincinnati/Northern Kentucky', 'Travis AFB', 129, 'Atlas Air'),
('7F4804', '2020-06-10', '10:45:00', 'Pangnirtung', 'Iqaluit', 110, 'First Air'),
('8C3202', '2020-06-10', '09:00:00', 'San Francisco International', 'Greater Rockford Airport', 160, 'ATI'),
('9M9370', '2020-06-10', '09:45:00', 'Comox', 'Terrace', 218, 'Central Mountain Air'),
('A81351', '2020-06-10', '07:30:00', 'Sky Harbor International', 'Albuquerque International', 148, 'Ameriflight'),
('A81924', '2020-06-10', '08:45:00', 'Roberts Field', 'Roseburg Municipal Airport', 103, 'Ameriflight'),
('A81964', '2020-06-10', '07:50:00', 'State', 'La Grande', 131, 'Ameriflight'),
('A81992', '2020-06-10', '08:45:00', 'La Grande', 'Kingsley Field', 91, 'Ameriflight'),
('A85231', '2020-06-10', '09:20:00', 'San Luis County Regional Airport', 'Bob Hope', 170, 'Ameriflight'),
('AF201', '2020-06-11', '00:25:00', 'Seoul (Incheon)', 'Charles De Gaulle', 194, 'Air France'),
('AF8421', '2020-06-11', '00:55:00', 'Seoul (Incheon)', 'Schiphol', 120, 'Air France'),
('AN41', '2020-06-10', '09:00:00', 'Los Angeles International', 'South Padre Is. International', 166, 'Advanced Air'),
('APC1974', '2020-06-10', '08:00:00', 'North Bend', 'Eugene', 215, 'Airpac Airlines'),
('AT9735', '2020-06-11', '01:25:00', 'Seoul (Incheon)', 'Doha International', 154, 'Royal Air Maroc'),
('CAO1050', '2020-06-10', '11:50:00', 'Dallas/Fort Worth International', 'Ted Stevens Anchorage International Airport', 219, 'Air China Cargo'),
('CNS12', '2020-06-10', '12:50:00', 'null', 'Vero Beach Municipal', 206, 'PlaneSense'),
('CNS181', '2020-06-10', '11:30:00', 'Pease International', 'Naples', 165, 'PlaneSense'),
('CNS612', '2020-06-10', '13:00:00', 'Dekalb-Peachtree', 'Port Columbus International', 187, 'PlaneSense'),
('CWA927', '2020-06-10', '10:30:00', 'Lethbridge', 'Calgary International Airport', 175, 'CanWest Air'),
('CX261', '2020-06-11', '00:05:00', 'Hong Kong International', 'Charles De Gaulle', 185, 'Cathay Pacific'),
('CZ307', '2020-06-11', '00:05:00', 'Guangzhou Baiyun International', 'Schiphol', 109, 'China Southern Airlines'),
('DV781', '2020-06-11', '07:15:00', 'Almaty', 'Karaganda', 138, 'SCAT Airlines'),
('EM8695', '2020-06-10', '07:30:00', 'Seattle-Tacoma International', 'Orcas Island', 187, 'Empire Airlines'),
('EM8699', '2020-06-10', '07:45:00', 'Seattle-Tacoma International', 'Bellingham', 182, 'Empire Airlines'),
('EY873', '2020-06-11', '00:30:00', 'Seoul (Incheon)', 'Abu Dhabi International', 94, 'Etihad Airways'),
('FTH200', '2020-06-10', '12:00:00', 'International', 'Pal-Waukee', 148, 'Mountain Aviation'),
('FTH517', '2020-06-10', '11:34:00', 'Warren County', 'Opa Locka', 174, 'Mountain Aviation'),
('G49300', '2020-06-10', '10:45:00', 'Provo', 'Williams Gateway', 205, 'Allegiant Air'),
('G52983', '2020-06-11', '00:10:00', 'Chongqing Jiangbei International', 'Lijiang', 125, 'China Express Air'),
('HRT242', '2020-06-10', '12:00:00', 'Lester B. Pearson International', 'Kitchener-Waterloo Regional', 187, 'Chartright Air'),
('HZ5653', '2020-06-11', '01:00:00', 'Irkutsk', 'Novyy', 206, 'Aurora'),
('IF3810', '2020-06-10', '10:00:00', 'El Paso International Airport', 'Gen Fierro Villalobos', 129, 'Gulf and Caribbean Cargo'),
('IRO843', '2020-06-10', '10:00:00', 'Houghton County', 'General Mitchell International', 94, 'CSA Air'),
('JA1113', '2020-06-10', '12:00:00', 'Cerro Moreno', 'Arturo Merino Benitez', 149, 'JetSMART'),
('JTL602', '2020-06-10', '12:06:00', 'Lincoln', 'Aspen', 108, 'Jet Linx Aviation'),
('K4801', '2020-06-10', '08:30:00', 'Ted Stevens Anchorage International Airport', 'Cincinnati/Northern Kentucky', 124, 'Kalitta Air'),
('KH226', '2020-06-10', '06:45:00', 'Kahului', 'Hilo International', 164, 'Aloha Air Cargo'),
('KI2', '2020-06-10', '08:00:00', 'Metropolitan Oak International', 'Sacramento Mather Airport', 148, 'Kaiser Air'),
('KL4300', '2020-06-11', '00:05:00', 'Guangzhou Baiyun International', 'Schiphol', 123, 'KLM'),
('KL836', '2020-06-11', '00:05:00', 'Singapore Changi', 'Schiphol', 139, 'KLM'),
('KL856', '2020-06-11', '00:55:00', 'Seoul (Incheon)', 'Schiphol', 136, 'KLM'),
('KO43', '2020-06-10', '08:20:00', 'Petersburg James A Johnson', 'Sitka', 131, 'ACE Air Cargo'),
('LH4921', '2020-06-11', '00:10:00', 'Haneda Airport', 'Frankfurt International Airport', 90, 'Lufthansa'),
('LKF81', '2020-06-10', '13:00:00', 'Dupage County', 'Kissimmee Gateway', 132, 'Aviation Advisor'),
('LXJ333', '2020-06-10', '09:46:00', 'Arapahoe Co', 'San Diego International Airport', 124, 'Flexjet'),
('LXJ348', '2020-06-10', '09:00:00', 'Van Nuys', 'Pal-Waukee', 215, 'Flexjet'),
('LXJ357', '2020-06-10', '12:00:00', 'Venice Municipalcipal', 'Burke Lakefront', 123, 'Flexjet'),
('LXJ361', '2020-06-10', '11:25:00', 'Charlotte Douglas', 'Pal-Waukee', 182, 'Flexjet'),
('LXJ366', '2020-06-10', '09:30:00', 'San Francisco International', 'null', 103, 'Flexjet'),
('LXJ381', '2020-06-10', '12:13:00', 'Charlotte Douglas', 'Love Field', 168, 'Flexjet'),
('LXJ382', '2020-06-10', '10:34:00', 'Arapahoe Co', 'Birmingham', 123, 'Flexjet'),
('LXJ411', '2020-06-10', '12:34:00', 'Mc Ghee Tyson', 'General Mitchell International', 116, 'Flexjet'),
('LXJ416', '2020-06-10', '10:30:00', 'Spirit Of St Louis', 'Scottsdale Municipalcipal', 103, 'Flexjet'),
('LXJ453', '2020-06-10', '08:30:00', 'San Diego International Airport', 'Jackson Hole', 152, 'Flexjet'),
('LXJ545', '2020-06-10', '09:30:00', 'Palm Springs Metropolitan Area', 'Sun Valley', 92, 'Flexjet'),
('LXJ583', '2020-06-10', '08:30:00', 'Van Nuys', 'Arapahoe Co', 160, 'Flexjet'),
('MAI190', '2020-06-10', '12:20:00', 'Salluit', 'Kuujjuarapik', 91, 'Max Aviation'),
('MAL7090', '2020-06-10', '12:00:00', 'Lester B. Pearson International', 'Calgary International Airport', 109, 'Morningstar Air Express'),
('MAL8052', '2020-06-10', '09:47:00', 'Vancouver International', 'Nanaimo', 181, 'Morningstar Air Express'),
('MO470', '2020-06-10', '12:00:00', 'Arviat', 'Thompson', 214, 'Calm Air International'),
('NDL321', '2020-06-10', '12:30:00', 'Michel Pouliot', 'Rimouski', 204, 'Chrono Aviation'),
('NDL432', '2020-06-10', '09:30:00', 'Sept-Iles', 'Rimouski', 140, 'Chrono Aviation'),
('NDL901', '2020-06-10', '12:39:00', 'Mont Joli', 'Québec', 128, 'Chrono Aviation'),
('NGF1818', '2020-06-10', '14:30:00', 'Hanscom Field', 'Robert Lafleur', 212, 'Angel Flight America'),
('NH203', '2020-06-11', '00:10:00', 'Haneda Airport', 'Frankfurt International Airport', 135, 'ANA'),
('OZ6889', '2020-06-11', '01:25:00', 'Seoul (Incheon)', 'Doha International', 202, 'Asiana Airlines'),
('PCM8658', '2020-06-10', '06:30:00', 'Sacramento International', 'Mc Namara Field', 157, 'West Air (USA)'),
('PCM8731', '2020-06-10', '07:15:00', 'Sacramento International', 'Arcata', 129, 'West Air (USA)'),
('PUL107', '2020-06-10', '11:20:00', 'Sioux Lookout', 'Fort Frances Municipal', 157, 'Ornge Air'),
('QR859', '2020-06-11', '01:25:00', 'Seoul (Incheon)', 'Doha International', 109, 'Qatar Airways'),
('QUE10', '2020-06-10', '13:15:00', 'Iles De La Madeleine', 'Québec', 167, 'Gouvernement Du Quebec'),
('RJE43', '2020-06-10', '11:30:00', 'Hilton Head', 'Cincinnati Municipal-Lunken Field', 166, 'Reynolds Jet Management'),
('SU5653', '2020-06-11', '01:00:00', 'Irkutsk', 'Novyy', 205, 'Aeroflot'),
('SY3003', '2020-06-10', '12:05:00', 'Cincinnati/Northern Kentucky', 'Tampa International', 176, 'Sun Country Airlines'),
('SY3005', '2020-06-10', '12:00:00', 'Cincinnati/Northern Kentucky', 'Ontario International', 122, 'Sun Country Airlines'),
('T4809', '2020-06-10', '07:30:00', 'Hilo International', 'Honolulu International', 197, 'Rhoades Aviation'),
('TGO998', '2020-06-10', '09:40:00', 'Vancouver International', 'Kamloops', 207, 'Transport Canada'),
('THU103', '2020-06-10', '12:30:00', 'Moosonee', 'Timmins', 139, 'Thunder Airlines'),
('THU850', '2020-06-10', '12:50:00', 'Sudbury', 'London International', 137, 'Thunder Airlines'),
('THU860', '2020-06-10', '11:50:00', 'Moosonee', 'Kingston', 183, 'Thunder Airlines'),
('THU880', '2020-06-10', '12:30:00', 'Puvirnituq', 'Iqaluit', 145, 'Thunder Airlines'),
('UJ208', '2020-06-10', '13:00:00', 'Willow Run', 'Sharjah International Airport', 202, 'USA Jet Airlines'),
('VTM628', '2020-06-10', '09:00:00', 'Air Terminal', 'Abraham Gonzalez International', 210, 'Aeronaves TSM'),
('WG9950', '2020-06-10', '10:00:00', 'Lester B. Pearson International', 'Litchfield', 214, 'Sunwing'),
('WIG8103', '2020-06-10', '12:30:00', 'Luis Munoz Marin International', 'Beef Island', 206, 'Wiggins Airways'),
('WQ3513', '2020-06-10', '11:40:00', 'Love Field', 'Ramon Villeda Morales International', 193, 'Swift Air'),
('WT475', '2020-06-10', '11:20:00', 'Sioux Lookout', 'Angling Lake', 184, 'Wasaya Airways'),
('WT807', '2020-06-10', '11:45:00', 'Big Trout', 'Pickle Lake', 217, 'Wasaya Airways');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Reservation`
--
ALTER TABLE `Reservation`
  ADD PRIMARY KEY (`pseudo`,`vol`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`clef`,`pseudo`);

--
-- Indexes for table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`pseudo`);

--
-- Indexes for table `Vols`
--
ALTER TABLE `Vols`
  ADD PRIMARY KEY (`vol`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
