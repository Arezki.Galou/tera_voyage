import React, { Component } from 'react';
import {Button,Table} from 'react-bootstrap';
import "./login.css";   
import axios from "axios";

export default class Vols extends Component{
    constructor(props){
        super(props);
        this.state ={
            vols:[],
            change : false
        }
        this.reservationVol = this.reservationVol.bind(this);
    }

    componentDidMount(){
        const url = new URLSearchParams();
        url.append("Clef",this.props.clef);
        axios.get("http://127.0.0.1:8080/ReservationVol/recuperation?"+url).then(res=>{
            this.setState({vols :res.data["vols"][0] })
            console.log(res.data)
         });    
    }

    reservationVol(vol){
        const url = new URLSearchParams();
        url.append("Clef",this.props.clef);
        url.append("Vol",vol);
        axios.get("http://127.0.0.1:8080/ReservationVol/reserver?"+url).then(res => {
            if(res.data["id"][0]===1){
                alert(res.data["Message"]);
            }else{
                alert(res.data["Cause"])
            }
        });
    }
    render(){
        return (<div className="vols">
             <Table striped bordered hover>
                <thead>
                    <tr>
                    <th>Nom Vol</th>
                    <th>date Vol</th>
                    <th>heure Vol</th>
                    <th>Aeroport de deppart </th>
                    <th>aeroport d'arrivée </th>
                    <th>nombre Place </th>
                    <th>compagnie</th>
                    <th> # </th>
                    </tr>
                </thead>
                <tbody>
                    {   this.state.vols.map((vol) =>
                        <tr>
                            <td>{vol["vol"]}</td>
                            <td>{vol["date"]}</td>
                            <td>{vol["heure"]}</td>
                            <td>{vol["aeroportDepart"]}</td>
                            <td>{vol["aeroportArrivee"]}</td> 
                            <td>{vol["nombrePlaces"]}</td>
                            <td>{vol["compagnie"]}</td>
                            <td> 
                                <Button variant="primary" type="submit" onClick={(event)=>{event.preventDefault();this.reservationVol(vol["vol"])}}>
                                    reserver 
                                </Button>
                            </td>
                        </tr>
                            
                    )  
                    }
                </tbody>
            </Table>
        </div>);
    }
}