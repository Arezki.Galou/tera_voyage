import React, { Component } from 'react';
import Principale from './Principale';
import Acceuil from './Acceuil';
import axios from 'axios';
export default class NavigationPannel extends Component{
    constructor(props){
        super(props);
        
        this.state = {
            page_courant : "Principale",
            clef : ""
        }
    
        this.changerPage = this.changerPage.bind(this);
        this.Connexion   = this.Connexion.bind(this);
        this.Deconnexion = this.Deconnexion.bind(this);

    }
   
    Connexion(clefSession){
        this.setState({clef:clefSession})
        this.forceUpdate();
    }

    Deconnexion(){
        const url = new URLSearchParams()
        url.append("clef",this.state.clef);
        axios.get("http://127.0.0.1:8080/ReservationVol/deconnexion?"+url)
        .then(res=>{
            if(res.data["id"][0] === 1 ){
                alert(res.data["Message"]);
            }else{
                alert(res.data["Cause"])
            }
        }
        );
        this.setState({clef:"",page_courant : "Principale"})
    }
    changerPage(vers_page){
        this.setState({page_courant : vers_page});
    }

    render(){
        return(<nav>
            {this.state.page_courant === "Principale" ? <Principale Connexion = {this.Connexion} changerPage ={this.changerPage}  ></Principale>:<Acceuil clef = {this.state.clef} changerPage={this.changerPage} Deconnexion={this.Deconnexion} ></Acceuil>} 
            </nav>
        )
    }

}