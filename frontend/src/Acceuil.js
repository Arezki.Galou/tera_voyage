import React, { Component } from 'react';
import {Button,Navbar,Nav,Form} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Acceuil.css';
import logo from './logo.png';
import Pub from './Pub';
import Recherche from './Recherche';
import Profile from './Profile';
import Vols from './Vols';
export default class Acceuil extends Component{

    constructor(props){
        super(props);
        this.state={
            champ : "Acceuil"
        }
    }


    changerChamp(champChoisie){
        this.setState({champ:champChoisie})
    }

    render(){   
       return (
        <div> 
                <div>
                <Navbar bg="dark" variant="dark">
                    <Navbar.Brand href="#home"><img className ="imageLogo"  alt ="logo" src ={logo} onClick={(event)=> {event.preventDefault();this.changerChamp("Acceuil")}}/></Navbar.Brand>
                    <Nav className="mr-auto">
                        <Nav.Link href="#features" onClick={(event)=> {event.preventDefault();this.changerChamp("Recherche")}}>Recherche</Nav.Link>
                        <Nav.Link href="#profile" onClick={(event)=> {event.preventDefault();this.changerChamp("Profile")}}>Profile</Nav.Link>
                        <Nav.Link href ="#Vols"onClick={(event)=> {event.preventDefault();this.changerChamp("Vols")}}>Vols</Nav.Link>
                    </Nav>
                    <Form inline>
                        <Button variant="outline-info" onClick={(event=>{event.preventDefault(); this.props.Deconnexion()})}>Deconnexion</Button>
                    </Form>
                </Navbar> 
                </div>
       {this.state.champ === "Acceuil" ?<Pub></Pub>:(this.state.champ === "Recherche" ? <Recherche clef ={this.props.clef}></Recherche>:(this.state.champ === "Profile" ? <Profile clef ={this.props.clef}></Profile>:<Vols clef ={this.props.clef}></Vols>))  } 
    </div>)
    }
}