import React, { Component } from 'react';
import "./login.css";
import axios from "axios";
import logo from "./logo.png";

export default class Principale extends Component{
    constructor(props){
        super(props);
        this.state = {
            img : ""
        }
        this.seConnecter=this.seConnecter.bind(this);
        this.Inscription = this.Inscription.bind(this);
        this.chargerImage= this.chargerImage.bind(this);
    }
    seConnecter(){
        const url=new URLSearchParams();
        url.append("pseudo",this.refs.pseudologin.value);
        url.append("motDePasse",this.refs.pass.value);
        axios.get("http://127.0.0.1:8080/ReservationVol/connexion?"+url)
        .then(res=>{
            console.log(res.data);
            if(res.data["id"][0] === 1){
                    alert(res.data["Message"]);
                    this.props.Connexion(res.data["Clef"]);
                    this.props.changerPage("Acceuil");
                }else{
                  alert(res.data["Cause"])
                  
                }
            }
            );
    }logo


    chargerImage(){
        const inpFile = document.getElementById("inpFile")
        const previewContainer = document.getElementById("imagePreview")    
        const previewImage = previewContainer.querySelector(".image-preview__image")
        const file = inpFile.files[0];
        if(file){
            const reader = new FileReader();
            const res= reader.addEventListener("load",function(){
                previewImage.setAttribute("src",this.result)
                previewImage.setAttribute("value",this.result);
                return this.result;
            })
            reader.readAsDataURL(file)
            console.log(res)
        }

        console.log(this.refs.i.value)
    }
    Inscription(){
        const url = new URLSearchParams();
        url.append("nom",this.refs.nom.value);
        url.append("prenom",this.refs.prenom.value);
        url.append("pseudo",this.refs.pseudo.value);
        url.append("email",this.refs.mail.value);
        url.append("motDePasse",this.refs.password.value);
        url.append("age",this.refs.age.value);
        url.append("telephone",this.refs.telephone.value);
        axios.get("http://127.0.0.1:8080/ReservationVol/inscription?"+url)
        .then(res=>{
            if(res.data["id"][0] === 1 ){
                alert(res.data["Message"]);
                this.props.Connexion(res.data["Clef"]);
                this.props.changerPage("Acceuil");
            }else{
                alert(res.data["Cause"])
            }
        }
        );
    }
    render(){
        return (
            <div>
            <form className="log">
                <input  type="Pseudo" ref="pseudologin" placeholder="Login" id="email"/>
                <input  type="password" ref="pass" placeholder="Password" id="password"/>
                <input type="Submit" onClick={(event)=>{event.preventDefault();this.seConnecter()}} defaultValue="Login" className="login"/> 
                
                <div className="formulaire">
                    <img src={logo}></img>
                    <input type="text" ref ="nom" placeholder="Nom..."/>
                    <input type="text" ref="prenom" placeholder="Prenom..."/>
                    <input type="mail" ref="pseudo" placeholder="pseudo..."/>
                    <input type="password" ref="password" placeholder="password..."/>
                    <input type="mail" ref="mail" placeholder="e-mail:exemple@toto.com  "/>
                    <input type="number" ref ="age" placeholder="age..."/>
                    <input type="tel" ref ="telephone" placeholder="0123456789..."/>
                    <input type="Submit" defaultValue="inscription" onClick={(event)=>{event.preventDefault();this.Inscription()}} 
                     className="login"/>
                </div>
            </form>
            </div>
        )
    }
}