import React, { Component } from 'react';
import {Form,Table,Button} from 'react-bootstrap';
import './Acceuil.css';
import axios from "axios";

export default class Recherche extends Component{
    constructor(props){
        super(props)
        this.state = {
            page_courante : "recherche",
            vols:[]
        }

        this.rechercher = this.rechercher.bind(this);
    }
    
    rechercher(){
        const url = new URLSearchParams();
        url.append("Clef",this.props.clef);
        url.append("dateVol",this.refs.dateVol.value)
        url.append("aeroportDepart",this.refs.aeroportDepart.value)
        url.append("aeroportArrivee",this.refs.aeroportArrivee.value)
        axios.get("http://127.0.0.1:8080/ReservationVol/recherche?"+url).then(res=>{
            if (res.data["id"][0] === 1){
                this.setState({page_courante :"vols" ,vols :res.data["vols"][0] })
                console.log(this.state.vols)
            }else{
                alert(res.data["Cause"])
            }   
        });
    }
    reservationVol(vol){
        const url = new URLSearchParams();
        url.append("Clef",this.props.clef);
        url.append("Vol",vol);
        axios.get("http://127.0.0.1:8080/ReservationVol/reserver?"+url).then(res => {
            if(res.data["id"][0]===1){
                alert(res.data["Message"]);
            }else{
                alert(res.data["Cause"])
            }
        });
    }
    render(){
        return (
        <div>
            { this.state.page_courante === "recherche" ?
            <div className="recherche" >
             <Form >
                <Form.Group controlId="formGroupPassword">
                    <Form.Label>Date Vol</Form.Label>
                    <Form.Control type="date" ref="dateVol" placeholder="Date vol" />
                </Form.Group>
                <Form.Group controlId="formGroupPassword">
                    <Form.Label>Aeroport de Départ</Form.Label>
                    <Form.Control type="text" ref="aeroportDepart" placeholder="Aeroport de Depart" />
                </Form.Group>
                <Form.Group controlId="formGroupPassword">
                    <Form.Label>Aeroport d'arrivé</Form.Label>
                    <Form.Control type="text" ref="aeroportArrivee" placeholder="Aeroport de arrivé" />
                </Form.Group>
                <Button variant="primary" type="submit" onClick={(event)=>{event.preventDefault();this.rechercher()}}>
                    Recherche
                </Button>
             </Form>
            </div>
            : 
            <div className="vols">
            <Table striped bordered hover>
               <thead>
                   <tr>
                   <th>Nom Vol</th>
                   <th>date Vol</th>
                   <th>Aeroport de deppart </th>
                   <th>aeroport d'arrivée </th>
                   <th> nombre Place </th>
                   <th> # </th>
                   </tr>
               </thead>
               <tbody>
                   {   this.state.vols.map((vol) =>
                       <tr>
                           <td>{vol["vol"]}</td>
                           <td>{vol["date"]}</td>
                           <td>{vol["aeroportDepart"]}</td>
                           <td>{vol["aeroportArrivee"]}</td> 
                           <td>{vol["nombrePlaces"]}</td>
                           <td> 
                               <Button variant="primary" type="submit" onClick={(event)=>{event.preventDefault();this.reservationVol(vol["vol"])}}>
                                   reserver 
                               </Button>
                           </td>
                       </tr>
                           
                   )  
                   }
                </tbody>
                </Table>
            </div> 
            }   
        </div> 
        );
    }
}