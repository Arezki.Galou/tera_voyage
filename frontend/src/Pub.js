import React, { Component } from 'react';
import {Carousel} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import koh from './kohphiphi.jpg';
import voyage from './voyage.jpg';
import maya from './tahiti.jpg';
import axios from "axios"
export default class Pub extends Component{

    componentDidMount(){
        axios.get("http://127.0.0.1:8080/ReservationVol/api?");
    }

    render(){
        return (
            <div className="slide">
            <Carousel className="image" >
            <Carousel.Item>
                    <img 
                    className="d-block w-100"
                    src={voyage}
                    alt="Qualité et Confort "
                    />
                    <Carousel.Caption>
                    <h3>Meilleur compagnie pour des destinations de reves</h3>
                    <p>les voyages forme la jeunesse, ne l'oublier jamais </p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img 
                    className="d-block w-100"
                    src={maya}
                    alt="Destination tahiti"
                    />
                    <Carousel.Caption>
                    <h3>Tahiti: Ile paradisiaque </h3>
                    <p>l'une des destinations les plus demandés pour vos vacances et pour pas cher avec nos compagnie!.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img 
                    className="d-block w-100"
                    src={koh}
                    alt="Kohphiphi"
                    />
                    <Carousel.Caption>
                    <h3>Koh Phi Phi </h3>
                    <p>une experiences incroyable au sein de nos compagnie pour l'une des distinations les plus belle du monde, venez decouvrir la thailande avec nous.</p>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
            </div>
        )
    }
}