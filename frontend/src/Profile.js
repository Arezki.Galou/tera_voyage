import React, { Component } from 'react';
import {Form,Button,InputGroup,FormControl,Table} from 'react-bootstrap';
import './Acceuil.css';
import axios from 'axios';
import {faBirthdayCake, faUser ,faEnvelopeSquare,faAddressBook } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import image from "./Image/profile.jpg";
export default class Profile extends Component{
    constructor(props){
        super(props);
        this.state = {
            isOpen : false,
            nom : "titi",
            prenom : "toto" ,
            age : 0 ,
            email : "exemple@gmail.com",
            telephone :"0123456789",
            pseudo : "toto15",
            reservation : [],
        }
        this.modificationMotdePasse = this.modificationMotdePasse.bind(this);
        this.toggle=this.toggle.bind(this);
    }

    toggle() {
        this.setState({isOpen : !this.state.isOpen})
    } 

    modificationMotdePasse(){
        const url = new URLSearchParams();
        url.append("Clef",this.props.clef);
        url.append("ancienMotDePasse",this.refs.ancien.value);
        url.append("nouveauMotDePasse",this.refs.nouveau.value);
        axios.get("http://127.0.0.1:8080/ReservationVol/modifier?"+url).then(res=>{
            if (res.data["id"][0] === 1 ){
                alert(res.data["Message"]);
                this.componentDidMount();
            }
            else
            {
                alert(res.data["Cause"]);
            }
        }
        );
    }
    annulationVol(vol){
        const url = new URLSearchParams();
        url.append("Clef",this.props.clef);
        url.append("Vol",vol);
        axios.get("http://127.0.0.1:8080/ReservationVol/annuler?"+url).then(res=>{
            if (res.data["id"][0] === 1 ){
                alert(res.data["Message"]);
                this.componentDidMount();
            }else{
                alert(res.data["Cause"]);
            }
            }
        );
    }
    componentDidMount(){
        const url = new URLSearchParams();
        url.append("Clef",this.props.clef);
        axios.get("http://127.0.0.1:8080/ReservationVol/profile?"+url)
        .then(res=>{
            console.log(res.data)
            if (res.data["id"][0]=== 1){
                this.setState({nom: res.data["nom"],prenom: res.data["prenom"],age: res.data["age"],email: res.data["email"],telephone: res.data["telephone"],pseudo: res.data["pseudo"],reservation:res.data["reservations"][0]})
            }else{
                alert(res.data["Cause"])
            }
        });
    }

    render(){
        return (
            
        <div className="profile">
            <div className="hautProfile">
                <div>
                    <Form className="changementMotDePasse">
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>nouveau Mot de Passe </Form.Label>
                        <Form.Control type="password" ref ="ancien" placeholder="nouveau Mot de Passe ...." />
                        <Form.Text className="text-muted">
                        We'll never share your email with anyone else.
                        </Form.Text>
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>ancien Mot de Passe</Form.Label>
                        <Form.Control type="password" ref ="nouveau"  placeholder="ancien Mot de Passe" />
                    </Form.Group>
                    <Button variant="primary" type="submit" onClick={(event)=>{event.preventDefault();this.modificationMotdePasse()}}>
                        Sauvegarder
                    </Button>
                </Form>
                </div>
                <div className="profile-card" >
                    <div className="image-container">
                        <img src={image}  alt="profile" />
                        <div className="title">
                            <h2>{this.state.nom}  {this.state.prenom}</h2>
                        </div>
                    </div > 
                    <div className="main-container">
                        <p>< FontAwesomeIcon className="icon" icon={faUser}/>{this.state.pseudo}</p><br/>
                        <p>< FontAwesomeIcon className="icon" icon={faEnvelopeSquare}/>{this.state.email}</p><br/>
                        <p><FontAwesomeIcon  className="icon" icon={faBirthdayCake}/>{this.state.age}</p><br/>
                        <p><FontAwesomeIcon  className="icon" icon={faAddressBook}/>{this.state.telephone}</p>
                    </div>
                </div>
            </div>
            <div className="myreservation">
                    <Table striped bordered hover>
                <thead>
                    <tr>
                    <th>Identifiant Vol</th>
                    <th>Date Vol</th>
                    <th>Horaire</th>
                    <th>Aeroport de depart </th>
                    <th>Aeroport d'arrivée </th>
                    <th>Compagnie</th>
                    <th> # </th>
                    </tr>
                </thead>
                <tbody>
                    {   this.state.reservation.map((vol) =>
                        <tr>
                            <td>{vol["vol"]}</td>
                            <td>{vol["date"]}</td>
                            <td>{vol["heure"]}</td>
                            <td>{vol["aeroportDepart"]}</td>
                            <td>{vol["aeroportArrivee"]}</td> 
                            <td>{vol["compagnie"]}</td>
                            <td> 
                                <Button variant="primary" type="submit" onClick={(event)=>{event.preventDefault();this.annulationVol(vol["vol"])}}>
                                    Annuler Vol
                                </Button>
                            </td>
                        </tr>
                            
                    )  
                    }
                </tbody>
            </Table>
             </div>
        </div>);
    }
}